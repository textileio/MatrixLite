// Copyright (c) 2017 Set. All rights reserved.

import PackageDescription

let package = Package(
    name: "MatrixLite",
    products: [
        .library(
            name: "MatrixLite",
            targets: ["MatrixLite"]
        ),
    ],
    targets: [
        .target(
            name: "MatrixLite",
            dependencies: []
        ),
        .testTarget(
            name: "MatrixLiteTests",
            dependencies: ["MatrixLite"]
        ),
    ]
)
