//
// Copyright (c) 2017 Set. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accelerate/Accelerate.h>

//! Project version number for MatrixLite.
FOUNDATION_EXPORT double MatrixLiteVersionNumber;

//! Project version string for MatrixLite.
FOUNDATION_EXPORT const unsigned char MatrixLiteVersionString[];
